// 1.1. Lista de países 
var countries =['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const listCountries = document.createElement('ul');

for (country of countries){
    var país = document.createElement('li');
    país.textContent = country;
    listCountries.appendChild(país);
}

document.body.appendChild(listCountries);

console.log(countries);

// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.

const claseRemove = document.querySelector('.fn-remove-me');
claseRemove.remove();

// 1.3. Lista coches
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];
const listLocation = document.querySelector('[data-function="printHere"]')
const listCars = document.createElement('ul');

for (car of cars){
    var carItem = document.createElement('li');
    carItem.textContent = car;
    listCars.appendChild(carItem);
}
listLocation.appendChild(listCars);

// 1.4. Crear lista con h4 + imagen
// var countries = [
// 	{title: 'Título 1', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
// 	{title: 'Título 2', imgUrl: 'https://picsum.photos/300/200?random=2'},
// 	{title: 'Título 3', imgUrl: 'https://picsum.photos/300/200?random=3'},
// 	{title: 'Título 4', imgUrl: 'https://picsum.photos/300/200?random=4'},
// 	{title: 'Título 5', imgUrl: 'https://picsum.photos/300/200?random=5'}
// ;
// for (country of countries){
//    var element = document.createElement('div');
//    element.innerHTML = `<h4>${country.title}</h4><img src=${country.imgUrl} />`;
//    document.body.appendChild(element);
// 

// 1.5. Botón delete
// 
// const itemList = document.querySelector('#delete-itemlist');
// 
// itemList.addEventListener("clik", function(){
//     const itemBorrar = document.querySelector('div');
//     itemBorrar[itemBorrar.length -1].remove();
// });

// 1.6. Botón delete para cada item
var countries = [
	{title: 'Título 1', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Título 2', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Título 3', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Título 4', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Título 5', imgUrl: 'https://picsum.photos/300/200?random=5'}
];
for (country of countries){
    var element = document.createElement('div');
    element.innerHTML = `<h4>${country.title}</h4><img src=${country.imgUrl} />`;

    var botonBorrar = document.createElement('button');
    botonBorrar.textContent = "Eliminar";
    element.appendChild(botonBorrar);

    botonBorrar.addEventListener("click", function() {
        element.remove()
    });

    document.body.appendChild(element);
}

